import {
  BadRequestException,
  Controller,
  Body,
  Post,
  Get,
  HttpCode,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @HttpCode(200)
  @Get('movies')
  async getAllMovie() {
    return this.appService.findAll();
  }

  @Post('movies')
  async createNew(@Body() body) {
    try {
      return await this.appService.createNew(body);
    } catch (err) {
      console.log(err);
      throw new BadRequestException(err);
    }
  }

  @Get('movies/:id')
  async findOne(@Param('id') id: number) {
    return this.appService.findById(id);
  }

  @Patch('movies/:id')
  async updateMovie(@Param('id') id: number, @Body() body) {
    try {
      return await this.appService.updateMovie(id, body);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  @Delete('movies/:id')
  async deleteMovie(@Param('id') id: number) {
    return await this.appService.deleteMovie(+id);
  }
}
