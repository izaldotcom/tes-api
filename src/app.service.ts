import { NotFoundException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
@Injectable()
export class AppService {
  constructor(private prismaClient: PrismaService) {}

  async createNew(params) {
    const paramsCreate = {
      title: params.title,
      description: params.description,
      rating: params.rating,
      image: params.image,
      created_at: new Date(),
      updated_at: null,
    };

    const result = await this.prismaClient.movie.create({
      data: paramsCreate,
    });

    return {
      status: 200,
      message: 'Success add new movie',
      data: result,
    };
  }

  async findAll() {
    const allData = await this.prismaClient.movie.findMany();

    if (allData) {
      return {
        status: 200,
        message: 'success',
        data: allData ?? {},
      };
    }
  }

  async findById(id: number) {
    const data = await this.prismaClient.movie.findFirst({
      where: {
        id: +id,
      },
    });

    if (data) {
      return {
        status: 200,
        message: 'success',
        data: data,
      };
    }
    throw new NotFoundException('Id not existed');
  }

  async updateMovie(id, params) {
    const paramsUpdate = {
      title: params.title,
      description: params.description,
      rating: params.rating,
      image: params.image,
      created_at: new Date(),
      updated_at: new Date(),
    };

    const result = await this.prismaClient.movie.update({
      where: {
        id: +id,
      },
      data: paramsUpdate,
    });

    return {
      status: 200,
      message: 'Success update a movie',
      data: result,
    };
  }

  async deleteMovie(id) {
    await this.prismaClient.movie.delete({
      where: {
        id: +id,
      }
    });

    return {
      status: 200,
      message: 'Success delete a movie'
    };
  }
}
